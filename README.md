# Angular Universal Test
The main feature for this app is to search images from unsplash.com using their rest API.
The results should appear as the user types in the angular application searchbar,
include at least one page with a static text sample Ex: Terms and conditions.

The goal for this test is to provide a fully functional Angular application including the following features.

  - Fully responsive
  - Store search terms in user session
  - Implement basic SEO tags
  - Implement pre-renderer script for static text page
  - For each URL pre-render generates html file using renderModuleFactory from @angular/platform-server module and saves generated html in appropriate location.

# Deployment
 Update this document with detailed instructions for the following tasks including any pre configuration needed
  - Build
  - Test
  - Deploy

# Warning
Single commit solution will not be considered.
